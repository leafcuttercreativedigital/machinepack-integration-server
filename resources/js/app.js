
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

const files = require.context('./', true, /\.vue$/i)

files.keys().forEach(key => {
    const name = key.split('/').pop().split('.')[0]
    Vue.component(name, files(key).default)
})

import MachinePack from 'machinepack-vue-ui-plugin'

try {
    Vue.use(MachinePack, {
        apiRoot: MachinePackVueSettings.API_ROOT
    })
} catch (e) {
    console.log('MachinePack disabled.', e)
}

const app = new Vue({
    el: '#app'
});
