<?php
Route::get('/', function () {
    $env = getenv(); // $_ENV not always set
    $settings = array_intersect_key($env, array_flip(preg_grep('/^MACHINEPACK_VUE_SETTINGS_/', array_keys($env))));
    return view('landing', [
        'MachinePackVueSettings' => array_combine(array_map(function($i) {
            return substr($i, strlen('MACHINEPACK_VUE_SETTINGS_'));
        }, array_keys($settings)), $settings)
    ]);
});
